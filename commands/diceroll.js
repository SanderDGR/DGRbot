const Discord = require('discord.js');
const embedcolor = '#1c69bc';
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

module.exports.run = (DiscordClient, message, args) => {
    // Create new Discord embed
    let embed = new Discord.RichEmbed()
        .setTitle(getRandomInt(1,6))
        .setColor(embedcolor);
    // Send the embed
    message.channel.send(embed);
}