const Discord = require('discord.js');
const urban = require('relevant-urban');

module.exports.run = async (DiscordClient, message, args) => {
    message.channel.startTyping();
    // Inform the user if no argument was given
    if(!args[0]) {
        message.channel.send('Please specify a search');
        message.channel.stopTyping();
        return;
    }
    let search = args.join(' ');
    let res = await urban(search).catch(e => {
        // Inform the user if the given word provided no results
        message.channel.send('That word was not found');
        message.channel.stopTyping();
        return;
    });
    // Create a new Discord embed
    let embed = new Discord.RichEmbed()
        .setURL(res.urbanURL)
        .setTitle(res.word)
        .setDescription(`**Definition:**\n${res.definition}\n\n**Example:**\n${res.example}`)
        .addField('Author', res.author)
        .addField('Rating', `:thumbsup: ${res.thumbsUp} **|** :thumbsdown: ${res.thumbsDown}`);
    // Send the embed
    message.channel.send(embed);
    message.channel.stopTyping();
}