const football = require('../footballdatahandler.js');

module.exports.run = (DiscordClient, message, args) => {
    let option = '';
    if(args[0]) option = args[0];
    switch(option.toLowerCase()) {
        case 'worldcup': {
            football.run(message, args, '2000');
            break;
        }
        case 'eredivisie' : {
            football.run(message, args, '2003');
            break;
        }
        case 'bundesliga': {
            football.run(message, args, '2002');
            break;
        }
        case 'premierleague': {
            football.run(message, args, '2021');
            break;
        }
        case 'championsleague': {
            football.run(message, args, '2001');
            break;
        }
        case 'seriea': {
            football.run(message, args, '2019');
            break;
        }
        case 'seriea_brazil': {
            football.run(message, args, '2013');
            break;
        }
        case 'ligue1': {
            football.run(message, args, '2015');
            break;
        }
        case 'primeradivision': {
            football.run(message, args, '2014');
            break;
        }
        default: {
            message.channel.send('Enter a valid league, please.');
            break;
        }
    }
}