const Discord = require('discord.js');

module.exports.run = (DiscordClient, message, args) => {
    // Create a new Discord embed
    let embed = new Discord.RichEmbed()
        .setTitle('DGRbot')
        .setDescription('A multi-purpose Discord bot. (Still being built)')
        .setThumbnail(DiscordClient.user.avatarURL)
        .setColor('#1c69bc')
        .addField('Source code and Documentation:', 'https://gitlab.com/SanderDGR/DGRbot')
        .setFooter('Version 0.0.5')
        .setTimestamp();

    // Send the embed
    message.channel.send(embed);
}