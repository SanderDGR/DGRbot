const Discord = require('discord.js');
const embedcolor = '#1c69bc';

// Get a random variable between the given min and max
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

module.exports.run = (DiscordClient, message, args) => {
    // Get 0 or 1 randomly
    let results = getRandomInt(0,1);
    let outcome;
    switch(results) {
        case 0: {
            outcome = 'Heads won!';
            break;
        }
        case 1: {
            outcome = 'Tails won!';
            break;
        }
    }
    // Create new Discord embed
    let embed = new Discord.RichEmbed()
        .setTitle(outcome)
        .setColor(embedcolor);
    // Send the embed
    message.channel.send(embed);
}