const Discord = require('discord.js');
const Snoowrap = require('snoowrap');
const reddithandler = require('../reddithandler.js');
module.exports.run = (DiscordClient, message, args) => {
    // Return the function if no second argument was given
    if(args[0] == null) {
        message.channel.send('Specify a subreddit, please');
        return;
    }
    reddithandler.redditRandom(DiscordClient, message, args);
}