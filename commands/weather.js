const Discord = require('discord.js');
const snekfetch = require('snekfetch');
require('dotenv').config();
const api = 'http://api.openweathermap.org/data/2.5/weather?q=';
const units = '&units=metric';
const appid = '&APPID=' + process.env.OWM_KEY;
const embedcolor = '#1c69bc';
module.exports.run = (DiscordClient, message, args) => {
    message.channel.startTyping();    
    if(args[0] == null) {
        message.channel.send('Specify a city first, please');
        message.channel.stopTyping();
        return;
    }
    let cityName = args.join(' ');
    // Fetch the weather data from OpenWeatherMap
    let weatherfetch = snekfetch.get(api + cityName + units + appid);
    weatherfetch.then(weather => {
        // Get the body from the snekfetch command
        wdata = weather.body;
        // Make a new Discord embed
        let embed = new Discord.RichEmbed()
            .setTitle('Weather for ' + wdata.name + ', ' + wdata.sys.country)
            .setDescription(wdata.weather[0].description)
            .setThumbnail('http://openweathermap.org/img/w/' + wdata.weather[0].icon + '.png')
            .addField('Temperature:', wdata.main.temp + ' C')
            .addField('Humidity:', wdata.main.humidity + '%')
            .setColor(embedcolor);
        // Send the embed
        message.channel.send(embed);
        message.channel.stopTyping();
    }).catch(e => {
        message.channel.send('Something went wrong. Please try again');
        message.channel.stopTyping();
        return;
    });
}