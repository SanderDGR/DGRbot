const Discord = require('discord.js');
const snekfetch = require('snekfetch');

module.exports.run = async (DiscordClient, message, args) => {
    let msg = await message.channel.send('Fetching dog...');
    message.channel.startTyping();
    // Get a random dog file from the random.dog api
    let randomdogdata = await snekfetch.get('https://random.dog/woof.json').catch(e => {
        message.channel.send('Something went wrong. Please try again');
        msg.delete();
        message.channel.stopTyping();
        return;
    });
    let randomdog = randomdogdata.body;
    let dogurl = randomdog.url;
    // Send the dog image file to Discord
    await message.channel.send({files: [
        {
            attachment: dogurl,
            name: dogurl.split('/').pop()
        }
    ]}).catch(e => {
        message.channel.send('Something went wrong. Please try again');
        msg.delete();
        message.channel.stopTyping();
        return;
    });
    // Delete the 'Fetching dog...' message from Discord
    msg.delete();
    message.channel.stopTyping();
}