const Discord = require('discord.js');
const snekfetch = require('snekfetch');

module.exports.run = async (DiscordClient, message, args) => {
    let msg = await message.channel.send('Fetching fox...');
    message.channel.startTyping();
    // Get a random fox image link from  randomfox.ca
    let randomfoxdata = await snekfetch.get('https://randomfox.ca/floof/').catch(e => {
        message.channel.send('Something went wrong. Please try again');
        msg.delete();
        return;
    });
    let randomfox = randomfoxdata.body;
    let foximg = randomfox.image;
    await message.channel.send({files: [
        {
            attachment: foximg,
            name: foximg.split('/').pop()
        }
    ]}).catch(e => {
        message.channel.send('Something went wrong. Please try again');
        msg.delete();
        return;
    });
    msg.delete();
    message.channel.stopTyping();
}