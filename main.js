const Discord = require('discord.js');
const DiscordClient = new Discord.Client();
require('dotenv').config();
const Snoowrap = require('snoowrap');
const fs = require('fs');
DiscordClient.commands = new Discord.Collection();
// Create a new Snoowrap client
const r = new Snoowrap({
    userAgent: 'reddit-bot-example-node',
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    username: process.env.REDDIT_USER,
    password: process.env.REDDIT_PASS
});

// Check the commands directory for JavaScript files
fs.readdir('./commands/', (err, files) => {
    // Check if for a .js extension
    let jsfiles = files.filter(f => f.split('.').pop() == 'js');
    // Check if there were no JavaScript files found
    if(jsfiles <= 0) {
        console.log('No JavaScript files found');
        return;
    }
    console.log('Loading ' + jsfiles.length + ' commands');
    // Include each command in the DiscordClient.commands collection
    jsfiles.forEach((f, i) => {
        let props = require('./commands/' + f);
        DiscordClient.commands.set(f, props);
    });
});

// TODO: Keep the code as D.R.Y. as possible
// Implement a Wikipedia command

const prefix = '!+'
const embedcolor = '#1c69bc';

// Fires this function when the Discord client is ready
DiscordClient.on('ready', () => {
    console.log(DiscordClient.commands);
    console.log('Discord client is ready.');
});

// This function fires after every message
DiscordClient.on('message', message => {
    // Checks if the message sent came from a bot. If so, the function will return
    if(message.author.bot == true) return;
    // Checks if the prefix is used. If it isn't, it will return the function
    if(!message.content.startsWith(prefix)) return;
    // Make the message lowercase
    message.content.toLowerCase();
    // Removes the prefix from the string and splits it up after each space
    let inputArray = message.content.substring(prefix.length).split(' ');
    // Takes the first word of the inputArrays
    let command = inputArray[0].toLowerCase();
    // Removes the first word from the inputArray and keeps the array
    let args = inputArray.slice(1);
    // Gets the command from the commands collection
    let cmd = DiscordClient.commands.get(command + '.js');
    // if cmd has a value, run the given command
    if(cmd) cmd.run(DiscordClient, message, args);
});

// Log the Discord client in to the Discord bot account
DiscordClient.login(process.env.DISCORD_TOKEN);