const Discord = require('discord.js');
const snekfetch = require('snekfetch');

// Get the league data from the football-data API
async function getLeagueInfo(leagueID) {
    let leagueInfoRaw = await snekfetch.get(`https://api.football-data.org/v2/competitions/${leagueID}`).set('X-Auth-Token', process.env.FOOTBALLAPIKEY).catch(e => {
        console.log(e);
        message.channel.stopTyping();
        return;
    });
    let leagueInfo = leagueInfoRaw.body;
    return leagueInfo;
}

// Get matches data from the football-data API from a specific league
async function getLatestMatches(leagueID) {
    // Get the latest matches from a specific league from the football-data API
    let latestMatchesRaw = await snekfetch.get(`https://api.football-data.org/v2/competitions/${leagueID}/matches?status=FINISHED`).set('X-Auth-Token', process.env.FOOTBALLAPIKEY).catch(e => {
        console.log(e);
        message.channel.stopTyping();
        return;
    });
    let latestMatches = latestMatchesRaw.body;
    // Reverse the matches array so that the latest matches will be the first in the array
    latestMatches.matches.reverse();
    return latestMatches;
}

// Get the latest standings from the football-data API
async function getStandingsInfo(leagueID) {
    let standingsRaw = await snekfetch.get(`https://api.football-data.org/v2/competitions/${leagueID}/standings`).set('X-Auth-Token', process.env.FOOTBALLAPIKEY).catch(e => {
        console.log(e);
        message.channel.stopTyping();
        return;
    });
    let standingsData = standingsRaw.body;
    return standingsData.standings
}

// Create a single string of league matches based on the latest football-data API
function createLatestMatchesText(matches, league) {
    let matchText = '';
    let penaltyText = '';
    for(i = 0; i < 5; i++) {
        if(matches[i] == undefined) break;       
        if(matches[i].score.duration == 'PENALTY_SHOOTOUT') penaltyText = `(Pentalties: [${matches[i].score.penalties.homeTeam} - ${matches[i].score.penalties.awayTeam}])`; 
        matchText = matchText + `${matches[i].homeTeam.name} **[${matches[i].score.fullTime.homeTeam} - ${matches[i].score.fullTime.awayTeam}]** ${matches[i].awayTeam.name} ${penaltyText}\n\n`;
        penaltyText = '';
    }
    if(matchText === '') return `${league.name} has no matches for this season yet.`;
    return matchText;
}




// Send the latest matches data to Discord
async function sendLatestMatches(leagueID, message) {
    let league = await getLeagueInfo(leagueID);
    let matchesData = await getLatestMatches(leagueID);
    let matches = matchesData.matches;
    let embed = new Discord.RichEmbed()
        .setTitle(`Latest ${league.name} results:`)
        .setDescription(createLatestMatchesText(matches, league));
    message.channel.send(embed);
    message.channel.stopTyping();
}

// Get the teams in proper ranking order from the standings array
function createStandingTeamText(standings) {
    let table = standings[0].table;
    let teamText = '';
    for(i = 0; i < table.length; i++) {
        teamText = teamText + `${table[i].position}. ${table[i].team.name}\n`;
    }
    return teamText;
}

// Get the standings data for each team
function createStandingsData(standings) {
    let table = standings[0].table;
    let standingData = '';
    for(i = 0; i < table.length; i++) {
        standingData = standingData + `[${table[i].playedGames} - ${table[i].won} - ${table[i].draw} - ${table[i].lost} - ${table[i].points}]\n`;
    }
    return standingData;
}

// Create a Discord embed with inline elements that contain the standings of a specific league
function createLeagueStandingsEmbed(league, standings) {
    let standingTeamText = createStandingTeamText(standings);
    let embed = new Discord.RichEmbed()
        .setTitle(`${league.name} standings:`)
        .addField('Team name', standingTeamText, true)
        .addField('[MP - W - D - L - Pts.]', createStandingsData(standings), true)
        .setFooter('This embed may be broken on mobile');
    return embed;
}

// Send the latest standings of league to Discord
async function sendStandings(leagueID, message) {
    let league = await getLeagueInfo(leagueID);
    let standings = await getStandingsInfo(leagueID);
    message.channel.send(createLeagueStandingsEmbed(league, standings));
    message.channel.stopTyping();
}


// TODO: Clean up code
module.exports.run = async (message, args, leagueID) => {
    let option = '';
    if(args[1]) option = args[1];
    message.channel.startTyping();
    switch(option.toLowerCase()) {
        case 'latest': {
            sendLatestMatches(leagueID, message,).catch(e => {
                message.channel.send('Something went wrong while getting the latest matches');
                message.channel.stopTyping();
                return;
            });
            break;
        }
        
        case 'standings': {
            sendStandings(leagueID, message).catch(e => {
                message.channel.send('Something went wrong while getting the latest standings');
                message.channel.stopTyping();
                return;
            });
            break;
        }
        // When none of the switch options are met, inform the user to use a valid command option
        default: {
            message.channel.send('Enter a valid league option, please.');
            message.channel.stopTyping();
            break;
        }
    }
}