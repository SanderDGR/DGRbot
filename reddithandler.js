const Discord = require('discord.js');
const Snoowrap = require('snoowrap');
const snekfetch = require('snekfetch');
const r = new Snoowrap({
    userAgent: 'reddit-bot-example-node',
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    username: process.env.REDDIT_USER,
    password: process.env.REDDIT_PASS
});
const embedcolor = '#1c69bc';

function redditPostHandler(redditpost, message) {
    redditpost.then(post => {
        // If the reddit post is NSFW, return the function
        if(post.over_18 == true) return;
        // If the url is undefined, inform the user and return the function
        if(post.url == undefined) {
            message.channel.send('That subreddit does not exist or the bot could not fetch the posts');
            return;
        }
        // sendRedditlink(message, post);
        postUrlHandler(message, post);
    });
}

// Send the image link of the reddit post and a direct reddit link
function sendRedditlink(message, post, postUrl) {
    // Checks if the url link is the same as the reddit link
    if(post.url == 'https://www.reddit.com' + post.permalink) {
        let embed = new Discord.RichEmbed()
            .setURL('https://www.reddit.com' + post.permalink)
            .setTitle(post.title)
            .setDescription(post.selftext)
            .setColor(embedcolor)
            .setFooter('Score: ' + post.score + ' | Upvote ratio: ' + post.upvote_ratio + ' | OP: ' + post.author.name);
        // Send the embed
        message.channel.send(embed);
        message.channel.stopTyping();
    } else {
        // Create a new embed with the details of the post
        let embed = new Discord.RichEmbed()
            .setURL('https://www.reddit.com' + post.permalink)
            .setColor(embedcolor)
            .setTitle(post.title)
            .setImage(postUrl)
            .setFooter('Score: ' + post.score + ' | Upvote ratio: ' + post.upvote_ratio + ' | OP: ' + post.author.name);
        // Send the embed
        message.channel.send(embed);
        message.channel.stopTyping();
    }
}

// Filters URL links if needed
async function postUrlHandler(message, post) {
    // Checks if the post.url is an undirect imgur link
    let isImgur = post.url.substring(8,17);
    if(isImgur == 'imgur.com') {
        let imglink = post.url;
        // Get the string after the last '/'
        let imgurID = imglink.split('/').pop();
        // Fetch the image data from imgur
        let imgurdata = await snekfetch.get('https://api.imgur.com/3/image/' + imgurID + '?client_id=' + process.env.IMGUR_ID).catch(e => {
            message.channel.send('Something went wrong. Please try again');
            message.channel.stopTyping();
            return;
        });
        imgurbody = imgurdata.body;
        sendRedditlink(message, post, imgurbody.data.link);
    } else {
        sendRedditlink(message, post, post.url);
    }
}

// Get a random post from the specified subreddit
function getRandomReddit(args) {
    let redditpost = r.getSubreddit(args[0]).getRandomSubmission();
    return redditpost;
}

//Get the newest post from the specified subreddit
function getNewReddit(args) {
    let redditpost = r.getSubreddit(args[0]).getNew()[0];
    return redditpost;
}

const redditErrorMsg = 'Something went wrong with Reddit. Please try again.';

module.exports = {
    redditRandom: function(DiscordClient, message, args) {
        message.channel.startTyping();
        let redditpost = getRandomReddit(args).catch(e => {
            // If an error occurs, log it to the console and inform the user
            console.log(e);
            message.channel.send(redditErrorMsg);
            message.channel.stopTyping();
            return;
        });
        redditPostHandler(redditpost, message);
    },
    redditNew: function(DiscordClient, message, args) {
        message.channel.startTyping();
        let redditpost = getNewReddit(args).catch(e => {
            // If an error occurs, log it to the console and inform the user
            console.log(e);
            message.channel.send(redditErrorMsg);
            message.channel.stopTyping();
            return;
        });
        redditPostHandler(redditpost, message);
    }

}